﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaMetodos
{
    class Program
    {
        static String nome;
        static float valorHora;
        static int horas;
        static void Main(string[] args)
        {
            int opc;
		    float bruto, ir, inss, fgts;
		    bool leitura=false;
		    do{
			    Console.WriteLine("\n\n1 - Ler dados");
			    Console.WriteLine("2 - Calcular salário líquido");
			    Console.WriteLine("3 - Sair");
                Console.Write("Opção: ");
			    opc = int.Parse(Console.ReadLine());
			    switch(opc){
				    case 1:
					    lerDados();
					    leitura = true;
					    break;
				    case 2:
					    if(leitura){
						    bruto = FolhaMetodos.calcularSalarioBruto(horas, valorHora);
						    inss = FolhaMetodos.calcularINSS(bruto);
						    ir = FolhaMetodos.calcularIR(bruto);
						    fgts = FolhaMetodos.calcularFGTS(bruto);
						    Console.WriteLine("\n\nSalário bruto: " + bruto);					
						    Console.WriteLine("INSS: " + inss);
						    Console.WriteLine("IR: " + ir);
						    Console.WriteLine("FGTS: " + fgts);
						    Console.WriteLine("Salário líquido: " + FolhaMetodos.calcularSalarioLiquido(bruto, inss, ir));
					    }
					    else{
                            Console.WriteLine("É necessário realizar a leitura dos dados.");
					    }
					    break;
			    }
		    }while(opc != 3);
        }

        private static void lerDados()
        {
            Console.Write("\nNome: ");
            nome = Console.ReadLine();
            Console.Write("Horas trabalhadas: ");
            horas = int.Parse(Console.ReadLine());
            Console.Write("Valor da hora: ");
            valorHora = float.Parse(Console.ReadLine());
        }

    }
}
